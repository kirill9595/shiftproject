FROM python:3.10-slim

ARG YOUR_ENV
ENV PYTHONFAULTHANDLER=1 \
  PYTHONUNBUFFERED=1 \
  PYTHONHASHSEED=random \
  PIP_NO_CACHE_DIR=off \
  PIP_DISABLE_PIP_VERSION_CHECK=on \
  PIP_DEFAULT_TIMEOUT=100 \
  POETRY_VERSION=1.1.14

RUN pip install "poetry==$POETRY_VERSION"

WORKDIR /ShiftProject
COPY poetry.lock /ShiftProject
COPY pyproject.toml /ShiftProject

# install poetry
RUN poetry config virtualenvs.create false \
  && poetry install $(test "$YOUR_ENV" == production && echo "--no-dev") --no-interaction --no-ansi

COPY . /ShiftProject

CMD ["uvicorn", "main:app", "--host", "0.0.0.0", "--port", "8000"]