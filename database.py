"""
This module is in use to connect to a SQLite database.

The database file should be located at the same directory.
"""

from sqlalchemy import create_engine
from sqlalchemy.orm import DeclarativeBase

SQLALCHEMY_DATABASE_URL = 'sqlite:///./shift.db'

engine = create_engine(
    SQLALCHEMY_DATABASE_URL, connect_args={'check_same_thread': False},
)


class Base(DeclarativeBase):
    pass
