
# ShiftProject

## Description
This app is a REST-service to view the user's current salary and the date of the next increase!  

Due to the fact that such data is very important and critical, each employee can only see his own amount.  

To ensure security, the method that produces a secret token which is valid for a certain period of time, based on the username and password of the employee, is implemented.  

Salary data is only given out when a valid token is shown.  

## Prerequisites
App is compatible with Python version 3.10.  
In addition, the FastAPI framework is required (tested and done on version 0.95.2).  
You will also need SQLAlchemy version 2 (tested with version 2.0.15)


## Installation

To run an application from the repository, you will need Git.

You can install it with [that](https://www.digitalocean.com/community/tutorials/how-to-install-git-on-ubuntu-20-04) instruction.  

Then you need to clone repository with the following command:

``` sh
$ git clone https://gitlab.com/kirill9595/shiftproject.git
```  
That will create directory __./ShiftProject__.  

### Running Locally
Then you can enter that dir with use of: 

``` sh
$ cd ./ShiftProject
```

And create virtual environment by using:  

``` sh
$ python -m venv venv
```

or   

``` sh
$ python3 -m venv venv
```

After creation of virtual environment you should install poetry

``` sh
$ pip install poetry==1.1.14
$ poetry config virtualenvs.create false
```

And install packages

``` sh
$ poetry install
```

To run the application, you must be in the directory where the main.py file is located.

The library is user-friendly and stars with command:  

``` sh
$ uvicorn main:app --reload
```

### Running with docker
You will need to install [Docker](https://docs.docker.com/engine/install/ubuntu/)  

And while in directory where Dockerfile (__/ShiftProject__) is located, build Docker image

``` sh
$ docker build .
``` 

You can find created Docker image with use of

``` sh
$ docker images
```

Then start container with command:  

``` sh
$ docker run --rm -p 8000:8000 <image_name>
```

## Usage
In both cases your app should be running on [http://127.0.0.1:8000/docs](http://127.0.0.1:8000/docs)
 
You will see the automatic interactive API documentation.  

![img_1.jpeg](https://gitlab.com/kirill9595/images/-/raw/main/photo1686579464.jpeg)

### Usage of `/get/token` method

While using the `/get/token` method you can use following credentials:  

``` sh
"username": kirill@user.com
"password": qwerty12345
```

or  

``` sh
"username": petr@user.com
"password": 12345qwerty
```

and you will get existing or newly created token.

![img_2.jpeg](https://gitlab.com/kirill9595/images/-/raw/main/photo1686580652.jpeg)

### Usage of `/get/salary` method

While using the `/get/salary` method you can use following tokens:

``` sh
"token_1": 58e07ed898c14948b6a48fc1ef5efa86
"token_2": 25c6dd2fc7f242feaf3e27e09a315842
"token_3": c44b5c25d71b4e69afc62c581c6c68fb
"token_4": 3bbf569165b04ae791f27565bea0b961
"token_5": a88831343aa5441db2d0523e22639db2
"token_6": 2baa294ec4864a87b27d9c32bdec89bd
``` 

and you will get salary info.

![img_2.jpeg](https://gitlab.com/kirill9595/images/-/raw/main/photo1686581083.jpeg)

## Troubleshooting
If database is empty, please remove it and run `create_db.py` module.

## Built With
* [FastAPI](https://fastapi.tiangolo.com/lo/) - The web framework used
* [Poetry](https://python-poetry.org/) - Dependency Management
* [SQLite](https://www.sqlite.org/index.html) - Used as database engine
* [SQLAlchemy](https://www.sqlalchemy.org/) - ORM for interaction with the database

## Authors and acknowledgment
* __[Kirill Iakovlev](https://gitlab.com/kirill9595)__

## License
This project is licensed under the MIT License - see the [LICENSE](https://gitlab.com/kirill9595/shiftproject/-/blob/test/LICENSE) file for details.

