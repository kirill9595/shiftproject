"""The module is used to create a database"""

from models import Base, Salary, Token, User
from database import engine
from sqlalchemy.orm import Session
Base.metadata.create_all(engine)

with Session(engine) as session:
    kirill = User(
        username="kirill@user.com",
        hashed_password="f540d7687157272467299964c0f39a0581664b1e05d73a152810db1e7bbcb795",
        tokens=[
            Token(value="58e07ed898c14948b6a48fc1ef5efa86", expired_at=1623214950),
            Token(value="25c6dd2fc7f242feaf3e27e09a315842", expired_at=1623214950),
            Token(value="c44b5c25d71b4e69afc62c581c6c68fb", expired_at=1717822504),
        ],
        salaries=Salary(amount=300000, increase_date="04.07.2023",),
    )
    petr = User(
        username="petr@user.com",
        hashed_password="753d52c7a0fc4c90f54764f543176cd2796571c0a5cd638b17a38871b8885e78",
        tokens=[
            Token(value="3bbf569165b04ae791f27565bea0b961", expired_at=1623214950),
            Token(value="a88831343aa5441db2d0523e22639db2", expired_at=1623214950),
            Token(value="2baa294ec4864a87b27d9c32bdec89bd", expired_at=1717822504),
        ],
        salaries=Salary(amount=250000, increase_date="05.07.2023",),
    )
    session.add_all([kirill, petr])
    session.commit()