"""This module create SQLAlchemy models from the Base class."""

from typing import List

from sqlalchemy import ForeignKey
from sqlalchemy.orm import Mapped, mapped_column, relationship

from database import Base


class User(Base):
    __tablename__ = 'users'

    id: Mapped[int] = mapped_column(primary_key=True)
    username: Mapped[str] = mapped_column(unique=True)
    hashed_password: Mapped[str]

    tokens: Mapped[List['Token']] = relationship(
        back_populates='user', cascade='all, delete-orphan',
    )

    salaries: Mapped['Salary'] = relationship(back_populates='user')


class Token(Base):
    __tablename__ = 'tokens'

    id: Mapped[int] = mapped_column(primary_key=True)
    value: Mapped[str]
    expired_at: Mapped[int]
    user_id: Mapped[int] = mapped_column(ForeignKey('users.id'))

    user: Mapped['User'] = relationship(back_populates='tokens')


class Salary(Base):
    __tablename__ = 'salaries'

    id: Mapped[int] = mapped_column(primary_key=True)
    amount: Mapped[int]
    increase_date: Mapped[str]
    user_id: Mapped[int] = mapped_column(ForeignKey('users.id'))

    user: Mapped['User'] = relationship(back_populates='salaries')
