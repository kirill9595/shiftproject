"""This module have reusable functions to interact with the database."""

import hashlib
import secrets
import time
import uuid

from sqlalchemy import insert, select

from database import engine
from models import Salary, Token, User

TOKEN_EXPIRES_IN = 31536000
PASSWORD_SALT = 'fa7a47cd4dc2f55889a999b8902a3d29e7836c91a187d624e31c1b98192fe6ed'


def check_if_user_in_database(username: str) -> int:
    """
    Connect to database and check if there is user with given username.
    If user is in database, function returns his id, else -- None.
    """
    select_user = select(User.id).where(User.username == username)
    with engine.connect() as conn:
        user_id = conn.execute(select_user).first()
    if user_id:
        return user_id[0]
    return user_id


def verify_password(username: str, password: str) -> bool:
    """
    Connect to database and check if user's password is correct.
    Function returns True if password is correct and False if not.
    """
    password_hash = hashlib.sha256((password + PASSWORD_SALT).encode()).hexdigest().lower()
    select_password = (
        select(User.hashed_password)
        .where(User.username == username)
    )
    with engine.connect() as conn:
        correct_password = conn.execute(select_password).first()[0]
    return secrets.compare_digest(password_hash, correct_password)


def check_token_by_username(username: str) -> str:
    """
    Connect to database and check if User have any valid tokens.
    Function return token value if it's in database or None if not.
    """
    select_token = (
        select(Token)
        .join(Token.user)
        .where(User.username == username)
        .where(Token.expired_at >= int(time.time()))
    )
    with engine.connect() as conn:
        valid_token = conn.execute(select_token).all()
    if valid_token:
        return valid_token[0][1]
    return valid_token


def create_token(user_id: int) -> str:
    """Function generate token and insert it into database."""
    token_expires_at = int(time.time()) + TOKEN_EXPIRES_IN
    user_token = uuid.uuid4().hex

    insert_token = insert(Token).values(
        value=user_token,
        expired_at=token_expires_at,
        user_id=user_id,
    )
    with engine.connect() as conn:
        conn.execute(insert_token)
        conn.commit()
    return user_token


def verify_token(token: str) -> int:
    """
    Connect to database and check if given token is valid.
    Function return user id if it's in database or None if not.
    """
    select_token = (
        select(Token.user_id)
        .where(Token.value == token)
        .where(Token.expired_at >= int(time.time()))
    )
    with engine.connect() as conn:
        user_id = conn.execute(select_token).first()
    if user_id:
        return user_id[0]
    return user_id


def get_salary(user_id: int) -> tuple:
    """
    Connect to database and get salary info, username by user id.
    Function return salary data if it's in database or None if not.
    """
    select_salary = (
        select(Salary, User.username)
        .join(Salary.user)
        .where(User.id == user_id)
    )
    with engine.connect() as conn:
        salary_data = conn.execute(select_salary).first()
    if salary_data:
        return salary_data[1], salary_data[2], salary_data[4]
    return salary_data
