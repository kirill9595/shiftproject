from typing import Annotated

from fastapi import Depends, FastAPI, Form, HTTPException, status
from fastapi.security import HTTPBasic, HTTPBasicCredentials

import crud
from database import engine
from models import Base

Base.metadata.create_all(engine)

app = FastAPI()

security = HTTPBasic()


@app.get('/get/token')
def get_token_by_username(credentials: Annotated[HTTPBasicCredentials, Depends(security)]):
    current_username = credentials.username
    current_password = credentials.password

    user_id = crud.check_if_user_in_database(current_username)

    if not user_id or not crud.verify_password(current_username, current_password):
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail='Incorrect username or password',
            headers={'WWW-Authenticate': 'Basic'},
        )

    current_token = crud.check_token_by_username(current_username)
    if current_token:
        return {'username': current_username, 'token': current_token}

    new_token = crud.create_token(user_id)
    return {'username': current_username, 'token': new_token}


@app.post('/get/salary')
def get_salary_info_by_token(current_token: str = Form(...)):
    current_user_id = crud.verify_token(current_token)
    if not current_user_id:
        return {
                'success': False,
                'message': 'It is not valid token, login with credentials to generate new.',
        }

    salary_info = crud.get_salary(current_user_id)
    if not salary_info:
        return {
                'success': False,
                'message': 'There are no salary info in database.',
        }

    return {
        'username': salary_info[2],
        'salary': salary_info[0],
        'increase_date': salary_info[1],
    }
